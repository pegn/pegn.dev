---
Title: PEGN, Parsing Expression Grammar Notation
---

PEGN heretically combines the best of
[PEG](https://bford.info/pub/lang/peg/), (A/E)BNF, and JSON in an
implementation-agnostic way (as nature intended). It is a parsing
expression grammar notation based on PEG but with natural-born
enhancements including some ABNF specificity and limit notation. A PEGN
grammar must embrace any implementation language unlike these many
well-intentioned but unnatural, dogmatic, language-specific sects:

* [Peg/Leg](https://www.piumarta.com/software/peg/)
* [Go PEG](https://github.com/pointlander/peg)
* [Python](https://medium.com/@gvanrossum_83706/peg-parsers-7ed72462f97c)
* [Pegen](https://github.com/we-like-parsers/pegen)
* [Pidgeon](https://github.com/mna/pigeon)
* [Pest.rs](https://pest.rs)

PEGN includes a standardized, compact JSON AST node tree model with a
"pretty" variation for grammar testing and debugging. Grammars are
documented in an additional Markdown file with code blocks containing
examples that double as input tests against other code blocks
containing the expected AST in pretty JSON form (inspired by the
CommonMark project). Markdown documentation can then be easily combined
with code generators of any kind.

## Origins

PEGN was conceived and created by [Rob Muhlestein](https://robs.io)
while creating grammars needed for the RWX KnowledgeNet specification
including Ezmark, Datamark, BaseQL, and others. During Rob's
[live-coding streams](https://twitch.com/rwxrob) others jumped in to
contribute providing valuable suggestions and merge requests. PEGN's
design has always been driven by practical needs making it useful for
other projects. It is designed for creating easy, sustainable, readable
grammars for any purpose. If you are planning anything involving any
sort of grammar you might consider taking a look at PEGN.

## Join Us

Perhaps you are a real grammar-nut and heretic at heart, like us? If so, come
join our cult/community. We welcome practicing our PEGN-ism with you.

## Coming Soon

* Vim Plugin
* Emacs Plugin
* VSCode Extension
* Language Syntax Server
* `pegn` Linter
* *Create Your First Language*
* Searchable Grammar Registry
